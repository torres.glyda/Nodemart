class Grocery {

  constructor() {
    this.cart = [];
    this.cash = 0;
    this.change = 0;
    this.isSenior = false;
    this.seniorDiscountRate = 0.05;
  }

  addItem(item, price) {
    this.cart.push({ item, price });
  }

  set totalPrice(total) {
    this.total = total;
  }

  get totalPrice() {
    return this.total;
  }

  computeTotalPrice() {
    this.totalPrice = this.cart.reduce((sum, cartItem) => sum + cartItem.price, 0);
  }

  computeChange() {
    this.change = this.cash - this.totalPrice;
  }

  checkout(cash, isSenior) {
    this.cash = cash;
    this.isSenior = isSenior;
    this.computeTotalPrice();
    this.computeChange();
    if (isSenior) {
      this.computeDiscountedTotalPrice();
    }
  }

  get seniorDiscount() {
    return this.totalPrice * this.seniorDiscountRate;
  }

  computeDiscountedTotalPrice() {
    this.totalPrice = this.totalPrice - this.seniorDiscount;
    this.computeChange();
  }

}

export default Grocery;