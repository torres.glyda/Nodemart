import Grocery from './Grocery';
import { expect } from 'chai';

describe('Grocery', () => {
  let grocery;

  beforeEach(() => {
    grocery = new Grocery();
  });

  describe('cart', () => {
    it('has empty cart', () => {
      expect(grocery.cart.length).to.equal(0);
    });

    it('adds item to cart', () => {
      grocery.addItem('Sunsilk 90 ml', 113.00);
      grocery.addItem('Tender Juicy', 45.00);
      expect(grocery.cart.length).to.equal(2);
    });
  });

  describe('totalPrice', () => {
    it('returns totalPrice', () => {
      grocery.addItem('Sunsilk 90 ml', 113.00);
      grocery.addItem('Tender Juicy', 45.00);
      grocery.computeTotalPrice();
      expect(grocery.totalPrice).to.be.closeTo(158.00, 0.01);
    });

    it('returns senior discounted total price', () => {
      grocery.addItem('Sunsilk 90 ml', 113.00);
      grocery.addItem('Tender Juicy', 45.00);
      grocery.checkout(200.00, true);
      // expect(grocery.discountedTotalPrice).to.be.closeTo(150.10, 0.01);
    });
  });

  describe('change', () => {
    it('returns change', () => {
      grocery.addItem('Sunsilk 90 ml', 113.00);
      grocery.addItem('Tender Juicy', 45.00);
      grocery.checkout(200.00);
      expect(grocery.change).to.be.closeTo(42.00, 0.01);
    });

    it('returns senior discounted change', () => {
      grocery.addItem('Sunsilk 90 ml', 113.00);
      grocery.addItem('Tender Juicy', 45.00);
      grocery.checkout(200.00, true);
      expect(grocery.change).to.be.closeTo(49.90, 0.01);
    });
  });

  describe('senior discount', () => {
    it('returns senior discount', () => {
      grocery.addItem('Sunsilk 90 ml', 113.00);
      grocery.addItem('Tender Juicy', 45.00);
      grocery.checkout(200.00, true);
      expect(grocery.seniorDiscount).to.be.closeTo(7.50, 0.01);
    });
  });

});